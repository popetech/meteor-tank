Orders = new Mongo.Collection("orders");

if (Meteor.isClient) {
  Meteor.subscribe("orders");

  // counter starts at 0
  Session.setDefault('counter', 0);

  Template.orders.helpers({
    orders: function () {
      return Orders.find({}, {sort: {createdAt: -1}});
    }
  });

  Template.addOrder.events({
    'click a': function () {
      Meteor.call("addOrder", 0);
    }
  });

  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });

  $.material.init();
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });

  Meteor.publish("orders", function () {
    return Orders.find({
      $or: [
        { "split.user": this.userId },
        { owner: this.userId }
      ]
    });
  });
}

Meteor.methods({
  addOrder: function (total) {
    // Make sure the user is logged in before inserting a order
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    Orders.insert({
      total: total,
      date: new Date(),
      createdAt: new Date(),
      owner: Meteor.userId(),
      username: Meteor.user().username
    });
  },
  deleteOrder: function (orderId) {
    var order = Orders.findOne(orderId);
    if (order.private && order.owner !== Meteor.userId()) {
      // If the order is private, make sure only the owner can delete it
      throw new Meteor.Error("not-authorized");
    }

    Orders.remove(orderId);
  },
  setChecked: function (orderId, setChecked) {
    var order = Orders.findOne(orderId);
    if (order.private && order.owner !== Meteor.userId()) {
      // If the order is private, make sure only the owner can check it off
      throw new Meteor.Error("not-authorized");
    }

    Orders.update(orderId, { $set: { checked: setChecked} });
  },
  setPrivate: function (orderId, setToPrivate) {
    var order = Orders.findOne(orderId);

    // Make sure only the order owner can make a order private
    if (order.owner !== Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    Orders.update(orderId, { $set: { private: setToPrivate } });
  }
});
