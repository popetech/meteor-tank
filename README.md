#Tank

Group Payments

* Create a new bill
* Invite others to bill
* Equal split
* Adjust amounts
* Allow others to input their amounts
* Approve input amounts
* Add tip
* Allow others to pay via app, track once paid back
* Allow bill creator to set terms of payback (fee, % with delay of timespan)
* Remind those that owe by clicking a button
* Show outstanding bills


Use less than 10 packages, only ones that are easy to use together

Use material design

    meteor add fezvrasta:bootstrap-material-design

Add to the head

    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>tank</title>

      <!-- Material Design fonts -->
      <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
      <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- Bootstrap -->


      <!-- Bootstrap Material Design -->
      <link href="dist/css/bootstrap-material-design.css" rel="stylesheet">
      <link href="dist/css/ripples.min.css" rel="stylesheet">

    </head>


Update the button to be a overlay/floating action button

    <a class="btn btn-fab btn-primary add-bill"><i class="material-icons">receipt</i></a>

SCSS

    meteor remove standard-minifiers
    meteor add seba:minifiers-autoprefixer
    meteor add fourseven:scss

tank.scss (rename or delete tank.css)

    $padding: 15px;

    .btn.add-bill {
      position: absolute;
      bottom: $padding;
      right: $padding;
    }


remove auto publish

    meteor remove autopublish
    meteor remove insecure
    meteor add ian:accounts-ui-bootstrap-3
    meteor add accounts-password
